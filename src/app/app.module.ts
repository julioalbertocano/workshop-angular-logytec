import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { SidebarComponent } from './components/shared/sidebar/sidebar.component';
import { RegistroComponent } from './components/registro/registro.component';
// services
import {RestPaisesService} from './services/rest-paises.service';
import { GrillaComponent } from './components/grilla/grilla.component';
// PIPES
import { NoTextPipe } from './pipes/no-text.pipe';
// RUTAS
import { APP_ROUTES } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    RegistroComponent,
    GrillaComponent,
    NoTextPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(APP_ROUTES, { useHash: true})
  ],
  providers: [RestPaisesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
