import { RouterModule, Routes } from '@angular/router';
import { RegistroComponent } from './components/registro/registro.component';
import { GrillaComponent } from './components/grilla/grilla.component';
export const APP_ROUTES: Routes = [
  { path: 'registro', component: RegistroComponent },
  { path: 'grilla', component: GrillaComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'grilla' }
];
