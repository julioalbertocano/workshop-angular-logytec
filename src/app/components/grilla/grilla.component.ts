import { Component, OnInit } from '@angular/core';
import { RestPaisesService } from '../../services/rest-paises.service';
import { Pais } from '../../model/pais.interface';

@Component({
  selector: 'app-grilla',
  templateUrl: './grilla.component.html',
  styleUrls: ['./grilla.component.css']
})
export class GrillaComponent implements OnInit {
  paises: Pais[];
  constructor(private _pais: RestPaisesService) {
    this.paises = this._pais.obtenerPaises();
  }

  ngOnInit() {
  }

}
