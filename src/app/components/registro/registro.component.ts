import { Component, OnInit } from '@angular/core';
import {Pais} from '../../model/pais.interface';
import { RestPaisesService } from '../../services/rest-paises.service';




@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  pais: Pais = {
    name: '',
    population: 0,
    capital: '',
    flag: ''
  };
  constructor(private _pais: RestPaisesService) { }

  ngOnInit() {
  }

  guardar() {
    this._pais.guardarPais(this.pais);
  }
}
