export interface Pais {
    name: string;
    population: number;
    capital?: string;
    flag?: string;
}
