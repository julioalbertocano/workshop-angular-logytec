import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noText'
})
export class NoTextPipe implements PipeTransform {

  transform(testo: any): string {
    return testo || 'Sin datos asignados';
  }

}
