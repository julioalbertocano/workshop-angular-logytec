import { Injectable } from '@angular/core';
import { Pais } from '../model/pais.interface';

@Injectable({
  providedIn: 'root'
})
export class RestPaisesService {

  constructor() {
    localStorage.setItem('paises', '[]');
   }

  guardarPais (pais: Pais) {
    let paises: Pais[] = JSON.parse(localStorage.getItem('paises'));
    paises.push(pais);
    localStorage.setItem('paises', JSON.stringify(paises));
  }

  obtenerPaises(): Pais[] {
    return JSON.parse(localStorage.getItem('paises'));
  }
}
